package com.abel.interfaces;

import java.util.List;

import com.abel.modelo.Empleado;



public interface EmpleadoCRUD {
	

  public List listar();
  public List listarByDNI(String dni);  
  public Empleado BuscarByID(String identifier);
  public void agregar(Empleado empleado);
  public void edit(Empleado empleado);
  public Boolean eliminar(String id);
  

}

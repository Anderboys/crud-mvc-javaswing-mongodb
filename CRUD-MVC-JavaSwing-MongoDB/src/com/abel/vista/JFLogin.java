package com.abel.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


import rsscalelabel.RSScaleLabel;

import java.awt.Color;
import java.awt.Label;
import java.awt.TextField;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class JFLogin extends JFrame {

	//private Image img_logo = new ImageIcon(Login.class.getResource("images/idatlogo.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	//private Image img_usu = new ImageIcon(Login.class.getResource("images/usu.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	//private Image img_pass = new ImageIcon(Login.class.getResource("images/pas.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	
	
	
	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;	
	private JLabel lblLoginmesage = new JLabel("");
	private JPasswordField txtpas;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 128), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.desktop);
		panel.setBounds(186, 171, 250, 40);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtUsername = new JTextField();
		txtUsername.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				
				if(txtUsername.getText().equals("Ander")) {
					txtUsername.setText("");
				}else {
					txtUsername.selectAll();
				}
				
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtUsername.getText().equals("")) {
					txtUsername.setText("UserName");
				}
			}
		});
		txtUsername.setBorder(null);
		txtUsername.setForeground(SystemColor.text);
		txtUsername.setBackground(SystemColor.desktop);
		txtUsername.setFont(new Font("Arial", Font.PLAIN, 12));
		txtUsername.setText("Username");
		txtUsername.setBounds(10, 11, 172, 20);
		panel.add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblusu = new JLabel("");
		lblusu.setBounds(194, 0, 46, 40);
		lblusu.setIcon(new ImageIcon(JFLogin.class.getResource("/com/abel/images/usuu.png")));
		panel.add(lblusu);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.desktop);
		panel_1.setBounds(186, 222, 250, 40);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		
		
		txtPassword = new JPasswordField();
		txtPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtPassword.getText().equals("abc123")) {
					txtPassword.setEchoChar('*');
					txtPassword.setText("");
				}else {
					txtPassword.selectAll();
				}
				
					
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtPassword.getText().equals("")) {
					txtPassword.setText("PassWord");
					txtPassword.setEchoChar((char)0);
				}
			}
		});
		txtPassword.setBorder(null);
		txtPassword.setForeground(SystemColor.text);
		txtPassword.setBackground(SystemColor.desktop);
		txtPassword.setFont(new Font("Arial", Font.PLAIN, 12));
		txtPassword.setText("Password");
		txtPassword.setBounds(10, 11, 173, 20);
		panel_1.add(txtPassword);
		
		
		
		
		
		
		JLabel lblpas = new JLabel("");
		lblpas.setBounds(200, 0, 46, 40);
		lblpas.setIcon(new ImageIcon(JFLogin.class.getResource("/com/abel/images/pass.png")));
		panel_1.add(lblpas);		
		
		
		
		JPanel btnLogin = new JPanel();
		btnLogin.addMouseListener(new MouseAdapter() {
			 
			@Override
			public void mouseClicked(MouseEvent e) {
				 
				 if(txtUsername.getText().equals("ander") && txtPassword.getText().equals("45089145")){
						lblLoginmesage.setText("");
						JOptionPane.showMessageDialog(null, "Succes !");
						JFPrincipal principal = new JFPrincipal();
						principal.setVisible(true);
						dispose();
						
					}else if(txtUsername.getText().equals("") || txtUsername.getText().equals("Username") ||
					         txtPassword.getText().equals("") || txtPassword.getText().equals("Password")) {
					
						lblLoginmesage.setText("Please input all requierements");
					}else {
						lblLoginmesage.setText("Username and password din,t match");
					}	
				 
			}
			 
			 
			 // EFECTO CAMBIO DE COLOR DEL BOON LOGIN
			@Override
			public void mouseEntered(MouseEvent e) {
				btnLogin.setBackground(new Color(30,60,60));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnLogin.setBackground(new Color(40,70,70));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btnLogin.setBackground(new Color(55,85,85));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btnLogin.setBackground(new Color(30,60,60));
			}			
			// ---------------------------------------
			
		});
		btnLogin.setBackground(SystemColor.activeCaptionText);
		btnLogin.setBounds(186, 290, 250, 49);
		contentPane.add(btnLogin);
		btnLogin.setLayout(null);
		
		
		// texto  LOG IN
		JLabel lblLogin = new JLabel("LOG IN");			
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setBackground(Color.BLACK);
		lblLogin.setFont(new Font("Arial", Font.BOLD, 14));
		lblLogin.setBounds(100, 11, 103, 27);
		btnLogin.add(lblLogin);
		
		
		
		JLabel lblcerrar = new JLabel("X");
		lblcerrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				if(JOptionPane.showConfirmDialog(null, "Seguro que Quieres Cerrar ?", "Confirmation", JOptionPane.YES_NO_OPTION)==0 ){
					JFLogin.this.dispose();
					}					
			}			
			@Override
			public void mouseEntered(MouseEvent e) {
				lblcerrar.setForeground(Color.RED);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lblcerrar.setForeground(Color.BLUE);
			}
			
			
			
		});
		lblcerrar.setForeground(Color.BLACK);
		lblcerrar.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblcerrar.setHorizontalAlignment(SwingConstants.CENTER);
		lblcerrar.setBounds(558, 7, 20, 20);
		contentPane.add(lblcerrar);
		
		JLabel lblidat = new JLabel("");
		lblidat.setIcon(new ImageIcon(JFLogin.class.getResource("/com/abel/images/idat.png")));
		lblidat.setBounds(186, 11, 210, 163);
		contentPane.add(lblidat);
		
		lblLoginmesage.setFont(new Font("Arial", Font.PLAIN, 12));
		lblLoginmesage.setForeground(new Color(255, 0, 0));
		lblLoginmesage.setBounds(186, 270, 250, 14);
		contentPane.add(lblLoginmesage);
		
	
		
		
				
		/*
		JLabel lblLogo = new JLabel("");
		lblLogoxD.setIcon(new ImageIcon(Login.class.getResource("/com/abel/images/idatlogo.png")));		
		//rsscalelabel.RSScaleLabel.setScaleLabel(lblLogo, "/com/abel/images/idatlogo.png");	
		lblLogo.setBounds(209, 11, 168, 154);
		contentPane.add(lblLogo);
		
		*/
		
		
		setLocationRelativeTo(null);


	}
}

package com.abel.vista;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.abel.modelo.Empleado;
import com.abel.modeloDAO.EmpleadoDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

import com.toedter.calendar.JCalendar;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JSeparator;

public class IFregistroEmp extends JInternalFrame implements ActionListener, PropertyChangeListener {
	private JLabel lblNewLabel;
	private JTextField txtIdentifierxD;
	private JLabel lblNewLabel_1;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JButton btnGuardar;

	
	Empleado emp = new Empleado();
	EmpleadoDAO edao = new EmpleadoDAO();
	private IFregistroEmp iFregistroEmp;
	private IFListarEmpleados ifListarEmpleados;
	Date date1;
	private JCalendar calendar1;
	private JTextField txtfechaxd;
	private JComboBox cboHabilitado;
	private JLabel lblNewLabel_6;
	private JTextField txtEliminar;
	private JButton btnNewButton;
	private JSeparator separator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFregistroEmp frame = new IFregistroEmp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFregistroEmp() {
		getContentPane().setBackground(Color.WHITE);
		GUI();	
	}		
	public void GUI() {

		setFrameIcon(new ImageIcon(IFregistroEmp.class.getResource("/com/abel/images/icon16/cliente2.png")));
		setClosable(true);
		setTitle("Registro Empleados");
		setBounds(100, 100, 450, 437);
		getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Identifier");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 11, 68, 14);
		getContentPane().add(lblNewLabel);
		
		txtIdentifierxD = new JTextField();
		txtIdentifierxD.setFont(new Font("Arial", Font.BOLD, 11));
		txtIdentifierxD.setBounds(10, 26, 86, 20);
		getContentPane().add(txtIdentifierxD);
		txtIdentifierxD.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(112, 11, 68, 14);
		getContentPane().add(lblNewLabel_1);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.BOLD, 11));
		txtNombre.setBounds(112, 26, 86, 20);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setFont(new Font("Arial", Font.BOLD, 11));
		txtApellido.setBounds(215, 26, 86, 20);
		getContentPane().add(txtApellido);
		txtApellido.setColumns(10);
		
		txtDni = new JTextField();
		txtDni.setFont(new Font("Arial", Font.BOLD, 11));
		txtDni.setBounds(322, 26, 86, 20);
		getContentPane().add(txtDni);
		txtDni.setColumns(10);
		
		lblNewLabel_2 = new JLabel("Fecha");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_2.setBounds(112, 57, 86, 14);
		getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Apellido");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_3.setBounds(215, 11, 86, 14);
		getContentPane().add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("DNI");
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_4.setBounds(328, 11, 80, 14);
		getContentPane().add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("Habilitado");
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_5.setBounds(10, 57, 86, 14);
		getContentPane().add(lblNewLabel_5);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Arial", Font.BOLD, 11));
		btnGuardar.addActionListener(this);
		btnGuardar.setBounds(7, 121, 89, 23);
		getContentPane().add(btnGuardar);
		
		calendar1 = new JCalendar();
		calendar1.addPropertyChangeListener(this);
		calendar1.getDayChooser().getDayPanel().addPropertyChangeListener(this);
		calendar1.setBounds(215, 64, 205, 153);
		getContentPane().add(calendar1);
		
		txtfechaxd = new JTextField();
		txtfechaxd.setFont(new Font("Arial", Font.BOLD, 11));
		txtfechaxd.setBounds(112, 79, 86, 20);
		getContentPane().add(txtfechaxd);
		txtfechaxd.setColumns(10);
		
		cboHabilitado = new JComboBox();
		cboHabilitado.setFont(new Font("Arial", Font.BOLD, 11));
		cboHabilitado.setModel(new DefaultComboBoxModel(new String[] {"Selecionar", "true", "false"}));
		cboHabilitado.setBounds(10, 78, 86, 22);
		getContentPane().add(cboHabilitado);
		
		lblNewLabel_6 = new JLabel("Eliminar Por Identifier");
		lblNewLabel_6.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_6.setBounds(10, 260, 142, 14);
		getContentPane().add(lblNewLabel_6);
		
		txtEliminar = new JTextField();
		txtEliminar.setBounds(10, 285, 117, 20);
		getContentPane().add(txtEliminar);
		txtEliminar.setColumns(10);
		
		btnNewButton = new JButton("Eliminar");
		btnNewButton.addActionListener(this);
		btnNewButton.setIcon(new ImageIcon(IFregistroEmp.class.getResource("/com/abel/images/icon16/eliminar (1).png")));
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 11));
		btnNewButton.setBounds(137, 285, 105, 23);
		getContentPane().add(btnNewButton);
		
		separator = new JSeparator();
		separator.setBounds(0, 247, 434, 2);
		getContentPane().add(separator);

	
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNewButton) {
			actionPerformedBtnNewButton(e);
		}
		if (e.getSource() == btnGuardar) {
			actionPerformedBtnGuardar(e);
		}
	}
	
	protected void actionPerformedBtnGuardar(ActionEvent e) {	
		
		registrar();
		
		JOptionPane.showMessageDialog(null, "Registrado Correctamente !");
		
		LimpiarCajas();
	}

	
// de Struture Components: Selecionar Calendar1 ->  add event Handller -> propertyChange
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if(evt.getOldValue() != null) {			
			SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd"); 
			txtfechaxd.setText(ff.format(calendar1.getCalendar().getTime()));
		}
		
	}
	
	public void registrar() {
		
		String ident = txtIdentifierxD .getText();
		String nom = txtNombre .getText();
		String ape = txtApellido .getText();
		String dni =txtDni .getText();
		Boolean enabled = Boolean.parseBoolean(cboHabilitado.getSelectedItem().toString());
		
		// PARSEAR DE STRING A DATE -->> CORREGIDO <<--
		String fecha = txtfechaxd.getText();
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}	
		
		emp.setIdentifier(ident);
		emp.setNombre(nom);
		emp.setApellido(ape);
		emp.setDni(dni);
		emp.setEnabled(enabled);
		emp.setIssueDate(date1);
//        e.setIssueDate(date2);
		edao.agregar(emp);
	}
	
	public void eliminar() {
		String ident = txtEliminar.getText();
		edao.eliminar(ident);
		

	}
	
	public void LimpiarCajas() {
		
		txtIdentifierxD.setText("");			
		txtIdentifierxD.requestFocusInWindow();
		txtNombre.setText("");
		txtApellido.setText("");
		txtDni.setText("");
		cboHabilitado.getSelectedIndex();
		txtfechaxd.setText("");	
		
	}
	
	protected void actionPerformedBtnNewButton(ActionEvent e) {
		eliminar();
		JOptionPane.showMessageDialog(null, "Eliminado Correctamente !");
		txtEliminar.setText("");
		txtEliminar.requestFocusInWindow();
		
		edao.IFListarEmpleados(ifListarEmpleados.tableempleados);
	}
}

package com.abel.vista;

import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import com.abel.modeloDAO.EmpleadoDAO;
import java.awt.BorderLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollBar;
import javax.swing.border.LineBorder;
import java.awt.Color;



public class IFListarEmpleados extends JInternalFrame {
	
	
	public JTable tableempleados;
	EmpleadoDAO edao = new EmpleadoDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFListarEmpleados frame = new IFListarEmpleados();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFListarEmpleados() {
		setBackground(Color.LIGHT_GRAY);
		
		
		//this.setExtendedState(Frame.MAXIMIZED_BOTH);

		setClosable(true);
		setFrameIcon(new ImageIcon(IFListarEmpleados.class.getResource("/com/abel/images/icon16/cliente2.png")));
		setTitle("Listar Empleados");
		setBounds(100, 100, 632, 487);
		getContentPane().setLayout(null);
		
		tableempleados = new JTable();
		tableempleados.setBackground(Color.WHITE);
		tableempleados.setBorder(new LineBorder(new Color(0, 0, 0)));
		tableempleados.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		tableempleados.setBounds(0, 11, 616, 446);
		getContentPane().add(tableempleados);
		
	


		// LISTAR DIRECTAMENTE
		edao.IFListarEmpleados(tableempleados);
		
	}

	
}

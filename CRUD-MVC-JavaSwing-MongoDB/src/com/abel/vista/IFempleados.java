package com.abel.vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 *
 * @author Fatima
 */
public class IFempleados extends JInternalFrame implements ActionListener {

	public JTextField txtIdentifier;
	public JTextField txtNombre;
	public JTextField txtApellido;
	public JTextField txtDNI;
	public JTextField txtEnabled;
	public JTable table;
	public JButton btnGuardar;
	public JButton btnEliminar;
	public JButton btnNuevo;
	JTextField txtFecha;
	private JLabel lblFechaRegistro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFempleados frame = new IFempleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFempleados() {
		super("Empleados", false, true, false, false);
		setFrameIcon(new ImageIcon(IFempleados.class.getResource("/com/abel/images/icon16/cliente.png")));
		setBounds(100, 100, 630, 340);
		getContentPane().setBackground(Color.WHITE);

		JLabel lblIdProducto = new JLabel("Identifier");

		txtIdentifier = new JTextField();
		txtIdentifier.setColumns(10);

		JLabel lblCodigo = new JLabel("Nombre");

		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		JLabel lblProducto = new JLabel("Apellido");

		txtApellido = new JTextField();
		txtApellido.setColumns(10);

		JLabel lblPrecio = new JLabel("DNI");

		txtDNI = new JTextField();
		txtDNI.setColumns(10);

		JLabel lblCantidad = new JLabel("Enable");

		txtEnabled = new JTextField();
		txtEnabled.setColumns(10);

		DefaultTableModel model = new DefaultTableModel();

		table = new JTable();

		table.setModel(model);

		JScrollPane jScrollPane = new JScrollPane(table);
		jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPane.setPreferredSize(new Dimension(420, 114));

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this);

		btnEliminar = new JButton("Eliminar");

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(this);

		txtFecha = new JTextField();
		
		txtFecha.setColumns(10);

		lblFechaRegistro = new JLabel("Fecha Registro");

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(lblIdProducto)
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(txtIdentifier, GroupLayout.PREFERRED_SIZE, 130,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnNuevo))
						.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout
								.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblProducto).addComponent(txtApellido,
														GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
										.addGap(4)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblPrecio).addComponent(txtDNI,
														GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)))
								.addComponent(lblCodigo)
								.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
								.addGap(3)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(txtFecha, GroupLayout.PREFERRED_SIZE, 138,
												GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(txtEnabled, GroupLayout.PREFERRED_SIZE, 138,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnGuardar)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnEliminar))
										.addComponent(lblCantidad).addComponent(lblFechaRegistro,
												GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))))
				.addContainerGap(14, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createSequentialGroup().addComponent(lblIdProducto)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(txtIdentifier,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addComponent(btnNuevo))
								.addGap(12).addComponent(lblCodigo).addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGroup(
								groupLayout.createSequentialGroup().addGap(51).addComponent(lblFechaRegistro)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(txtFecha, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblProducto)
						.addComponent(lblPrecio).addComponent(lblCantidad))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(txtDNI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(txtEnabled, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(btnGuardar).addComponent(btnEliminar))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		getContentPane().setLayout(groupLayout);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnGuardar) {
			actionPerformedBtnGuardar(e);
		}
		if (e.getSource() == btnNuevo) {
			actionPerformedBtnNuevo(e);
		}
	}

	protected void actionPerformedBtnNuevo(ActionEvent e) {
	}

	protected void actionPerformedBtnGuardar(ActionEvent e) {
	}
}

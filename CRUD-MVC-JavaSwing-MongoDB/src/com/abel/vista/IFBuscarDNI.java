package com.abel.vista;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

import org.eclipse.swt.internal.ole.win32.GUID;

import com.abel.modeloDAO.EmpleadoDAO;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class IFBuscarDNI extends JInternalFrame implements ActionListener {
	private JTextField txtIdentifier2;
	private JButton btnBuscar;

	public JTable tableDNI;
	EmpleadoDAO edao = new EmpleadoDAO();
	private JLabel lblNewLabel;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFBuscarDNI frame = new IFBuscarDNI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFBuscarDNI() {
		GUID();

	}
	
	void GUID() {
		setTitle("Buscar empleados por DNI");
		setFrameIcon(new ImageIcon(IFBuscarDNI.class.getResource("/com/abel/images/icon16/licencia-de-conducir.png")));
		setClosable(true);
		setBounds(100, 100, 546, 413);
		getContentPane().setLayout(null);
		
		txtIdentifier2 = new JTextField();
		txtIdentifier2.setBackground(new Color(255, 255, 255));
		txtIdentifier2.setForeground(SystemColor.activeCaptionText);
		txtIdentifier2.setBounds(10, 27, 243, 20);
		getContentPane().add(txtIdentifier2);
		txtIdentifier2.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this);
		btnBuscar.setBounds(275, 26, 89, 23);
		getContentPane().add(btnBuscar);
		
		tableDNI = new JTable();
		tableDNI.setBounds(10, 58, 510, 314);
		getContentPane().add(tableDNI);
		
		lblNewLabel = new JLabel("Ingrese DNI");
		lblNewLabel.setBounds(10, 11, 77, 14);
		getContentPane().add(lblNewLabel);
	}


	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnBuscar) {
			actionPerformedBtnBuscar(e);
		}
	}
	protected void actionPerformedBtnBuscar(ActionEvent e) {
		edao.IFListarPorDNIEmpleados(tableDNI, txtIdentifier2.getText());
	}
}

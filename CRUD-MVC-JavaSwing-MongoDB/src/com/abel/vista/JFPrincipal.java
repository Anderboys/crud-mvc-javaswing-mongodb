package com.abel.vista;

import java.awt.EventQueue;
import java.awt.Frame;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.JToolBar;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.abel.modeloDAO.EmpleadoDAO;
import com.abel.modelo.Empleado;

public class JFPrincipal extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JMenu MenuMantenimiento;
	private JMenu MenuEmpleados;
	private JMenu MenuSearch;
	private JMenuItem mtmProductos;
	private JMenuItem mntmClientes;
	private JMenuItem mntmEmpleados;
	private JMenu mnNewMenu;
	private JMenuItem mntmListarEmpleados;
	private JMenuItem mntmAcercaDe;
	private JToolBar toolBar; 
	private JButton btnEmpleados;

	// variables internal Frame
	private IFempleados jiFempleados;
	private IFproductos iFproductos;
	private IFBuscarDNI jifBuscarDNI;
	private IFregistroEmp iFregistroEmp;
	private IFListarEmpleados ifListarEmpleados;

	private JDesktopPane dpPrincipal;
	private JButton btnBuscarDNI;
	private JMenuItem mntmNewMenuItem;
	private JButton btnProduct;

	// JDesktopPane escritorio;
	private IFempleados iFempleados;
	Empleado emp = new Empleado();
	EmpleadoDAO edao = new EmpleadoDAO();

	
	private IFregistroEmp  ifRegistroEmp;

	
	Date date1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFPrincipal frame = new JFPrincipal();
					// maximiza la ventana
					frame.setExtendedState(MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	void dise�oGUI() {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(JFPrincipal.class.getResource("/com/abel/images/icon16/hogar.png")));
		setTitle("Java Swing Con MongoDB Atlas");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		toolBar = new JToolBar();
		toolBar.setFloatable(false);

		btnEmpleados = new JButton("Empleados");
		btnEmpleados.addActionListener(this);
		btnEmpleados.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon32/gato.png")));
		toolBar.add(btnEmpleados);

		dpPrincipal = new JDesktopPane();
		dpPrincipal.setBackground(SystemColor.desktop);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
				.addComponent(dpPrincipal, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(dpPrincipal, GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)));

		btnBuscarDNI = new JButton("Buscar por DNI");
		btnBuscarDNI.addActionListener(this);

		btnProduct = new JButton("Productos");
		btnProduct.addActionListener(this);
		btnProduct.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon32/producto.png")));
		toolBar.add(btnProduct);
		btnBuscarDNI.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon32/documento1.png")));
		toolBar.add(btnBuscarDNI);
		contentPane.setLayout(gl_contentPane);

		JMenu MenuSistema = new JMenu("Sistema");
		MenuSistema.setMnemonic('S');
		MenuSistema.setIcon(null);
		menuBar.add(MenuSistema);

		mntmNewMenuItem = new JMenuItem("Salir");
		mntmNewMenuItem.addActionListener(this);
		mntmNewMenuItem.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon16/logout.png")));
		MenuSistema.add(mntmNewMenuItem);

		MenuMantenimiento = new JMenu("Mantenimiento");
		MenuMantenimiento.setIcon(null);
		MenuMantenimiento.addActionListener(this);
		MenuMantenimiento.setMnemonic('P');
		menuBar.add(MenuMantenimiento);

		mtmProductos = new JMenuItem("Productos");
		mtmProductos.addActionListener(this);
		mtmProductos.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon16/paquete.png")));
		MenuMantenimiento.add(mtmProductos);

		mntmClientes = new JMenuItem("Clientes");
		mntmClientes.addActionListener(this);
		mntmClientes.setIcon(
				new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon16/servicio-al-cliente (1).png")));
		MenuMantenimiento.add(mntmClientes);

		mntmEmpleados = new JMenuItem("Empleados");
		mntmEmpleados.addActionListener(this);
		mntmEmpleados.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon16/cliente2.png")));
		MenuMantenimiento.add(mntmEmpleados);

		MenuEmpleados = new JMenu("Consultas");
		MenuEmpleados.setIcon(null);
		MenuEmpleados.addActionListener(this);
		MenuEmpleados.setMnemonic('E');
		menuBar.add(MenuEmpleados);

		mntmListarEmpleados = new JMenuItem("Listar Empleados");
		mntmListarEmpleados.addActionListener(this);
		mntmListarEmpleados
				.setIcon(new ImageIcon(JFPrincipal.class.getResource("/com/abel/images/icon16/cliente.png")));
		MenuEmpleados.add(mntmListarEmpleados);

		MenuSearch = new JMenu("Search By DNI");
		MenuSearch.addActionListener(this);
		MenuSearch.setMnemonic('S');
		menuBar.add(MenuSearch);

		JMenu mntmReporte = new JMenu("Reporte");
		mntmReporte.setMnemonic('R');
		menuBar.add(mntmReporte);

		mnNewMenu = new JMenu("Ayuda");
		menuBar.add(mnNewMenu);

		mntmAcercaDe = new JMenuItem("Acerca De ...");
		mntmAcercaDe.addActionListener(this);
		mnNewMenu.add(mntmAcercaDe);

	}

	public JFPrincipal() {
		this.setExtendedState(Frame.MAXIMIZED_BOTH);

		dise�oGUI();

	}


	public void limpiaProductoCampos(IFempleados iFempleados) {
		iFempleados.txtIdentifier.setText("");
		iFempleados.txtIdentifier.requestFocusInWindow();
		iFempleados.txtNombre.setText("");
		iFempleados.txtApellido.setText("");
		iFempleados.txtDNI.setText("");
		iFempleados.txtEnabled.setText("");
		iFempleados.txtFecha.setText("");

	}

	// carga formulario JIFempleados dentro JDesktopPane dpPrincipal
	void cargaEmpleados() {
		if (jiFempleados == null || jiFempleados.isClosed()) {
			jiFempleados = new IFempleados();
			dpPrincipal.add(jiFempleados);

			// centrar el internal frame
			Dimension tf = jiFempleados.getSize();
			jiFempleados.setLocation((dpPrincipal.getWidth() - tf.width) / 2,
					(dpPrincipal.getHeight() - tf.height) / 2);

			jiFempleados.show();
		}
	}
	
	void cargaIFRegistroEmpleados() {
		if (iFregistroEmp == null || iFregistroEmp.isClosed()) {
			iFregistroEmp = new IFregistroEmp();
			dpPrincipal.add(iFregistroEmp);

			// centrar el internal frame
			Dimension tf = iFregistroEmp.getSize();
			iFregistroEmp.setLocation((dpPrincipal.getWidth() - tf.width) / 2, (dpPrincipal.getHeight() - tf.height) / 2);

			iFregistroEmp.show();
		}
	}
	
	
	void cargaIFListarEmpleados() {
		if (ifListarEmpleados == null || ifListarEmpleados.isClosed()) {
			ifListarEmpleados = new IFListarEmpleados();
			dpPrincipal.add(ifListarEmpleados);

			// centrar el internal frame
			Dimension tf = ifListarEmpleados.getSize();
			ifListarEmpleados.setLocation((dpPrincipal.getWidth() - tf.width) / 2,
					(dpPrincipal.getHeight() - tf.height) / 2);

			ifListarEmpleados.show();
		}
	}
	

	// carga formulario JIFsearchByDNI dentro JDesktopPane dpPrincipal

	void cargaIFBuscarDNI() {
		if (jifBuscarDNI == null || jifBuscarDNI.isClosed()) {
			jifBuscarDNI = new IFBuscarDNI();
			dpPrincipal.add(jifBuscarDNI);

			// centrar el internal frame
			Dimension tf2 = jifBuscarDNI.getSize();
			jifBuscarDNI.setLocation((dpPrincipal.getWidth() - tf2.width) / 2,
					(dpPrincipal.getHeight() - tf2.height) / 2);

			jifBuscarDNI.show();
		}
	}

	void cargaIFProductos() {
		
		if (iFproductos == null || iFproductos.isClosed()) {
			iFproductos = new IFproductos();
			dpPrincipal.add(iFproductos);

			// centrar el internal frame
			Dimension tf2 = iFproductos.getSize();
			iFproductos.setLocation((dpPrincipal.getWidth() - tf2.width) / 2,
					(dpPrincipal.getHeight() - tf2.height) / 2);

			iFproductos.show();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mntmListarEmpleados) {
			actionPerformedMntmConsultaEmpleado(e);
		}
		if (e.getSource() == btnProduct) {
			actionPerformedBtnProduct(e);
		}

		if (e.getSource() == mntmNewMenuItem) {
			actionPerformedMntmNewMenuItem(e);
		}

		if (e.getSource() == btnBuscarDNI) {
			actionPerformedBtnBuscarDNI(e);
		}

		if (e.getSource() == btnEmpleados) {
			actionPerformedBtnClientes(e);
		}

		if (e.getSource() == mntmClientes) {
			actionPerformedMntmClientes(e);
		}

		if (e.getSource() == mntmEmpleados) {
			actionPerformedMnNewMenu(e);
		}

		if (e.getSource() == mntmAcercaDe) {
			actionPerformedMntmNewMenuItem_3(e);
		}

		if (e.getSource() == MenuSearch) {
			actionPerformedMnNewMenu_2(e);
		}
		if (e.getSource() == MenuEmpleados) {
			actionPerformedMnNewMenu_1(e);
		}
		if (e.getSource() == MenuMantenimiento) {
			actionPerformedMnNewMenu(e);
		}
	}

	protected void actionPerformedMnNewMenu(ActionEvent e) {
		cargaIFRegistroEmpleados();
	}

	protected void actionPerformedMnNewMenu_1(ActionEvent e) {
	}

	protected void actionPerformedMnNewMenu_2(ActionEvent e) {
	}

	protected void actionPerformedMntmClientes(ActionEvent e) {
		// llamando el metodo cargaEmpleados() para carga el JframeInternal
		cargaEmpleados();
	}

	protected void actionPerformedMntmNewMenuItem_3(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "Desarrolado por A Calcina\n email: anderporsiempre@gmail.com", "Acerca de",
				JOptionPane.INFORMATION_MESSAGE);
	}

	protected void actionPerformedBtnClientes(ActionEvent e) {
		
		cargaEmpleados();
	
	}

	protected void actionPerformedBtnBuscarDNI(ActionEvent e) {
		cargaIFBuscarDNI();
	}

	protected void actionPerformedMntmNewMenuItem(ActionEvent e) {
		JFLogin login = new JFLogin();
		login.setVisible(true);
		dispose();
	}

	protected void actionPerformedBtnProduct(ActionEvent e) {
		cargaIFProductos();
	}
	protected void actionPerformedMntmConsultaEmpleado(ActionEvent e) {
		cargaIFListarEmpleados();
	}
}

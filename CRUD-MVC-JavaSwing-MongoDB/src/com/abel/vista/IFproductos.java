package com.abel.vista;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.ImageIcon;

public class IFproductos extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFproductos frame = new IFproductos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFproductos() {
		setTitle("Productos");
		setFrameIcon(new ImageIcon(IFproductos.class.getResource("/com/abel/images/icon16/abrir-caja.png")));
		getContentPane().setBackground(UIManager.getColor("Button.disabledShadow"));
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

	}
}

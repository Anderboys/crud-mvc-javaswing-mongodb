package com.abel.modeloDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.abel.interfaces.EmpleadoCRUD;
import com.abel.modelo.Empleado;
import com.abel.vista.IFListarEmpleados;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import config.DBConfig;
import org.bson.Document;
import org.bson.conversions.Bson;

public class EmpleadoDAO implements EmpleadoCRUD {

	DBConfig dbConfig = new DBConfig();
	MongoDatabase DBO = null;
	Empleado emp = new Empleado();

	Date date1;
	java.sql.Date date2;

	public void IFListarEmpleados(JTable tabla) {

		// DefaultTableModel model;
		// String[] columnas = { "Identifier", "Nombre", "Apellido", "DNI",
		// "Habilitado", "FechaRegistro" };
		// model = new DefaultTableModel();

		/*
		 * DefaultTableModel model = new DefaultTableModel();
		 * model.addColumn("Identifier"); model.addColumn("Nombre");
		 * model.addColumn("Apellido"); model.addColumn("DNI");
		 * model.addColumn("Habilitado"); model.addColumn("Fecha Registro");
		 */

		DefaultTableModel model = new DefaultTableModel(null,
				new String[] { "Identifier", "Nombre", "Apellido", "DNI", "Habilitado", "Fecha Registro"

				});

		DBO = dbConfig.CNXMongoAtlas();
		if (DBO != null) {
			System.out.println("Conexion Ok");
		} else {
			System.out.println("Error Conexi�n");
		}

		Document document = null;
		MongoCursor<Document> result = DBO.getCollection("empleado").find().iterator();
		System.out.println(result);

		while (result.hasNext()) {

			document = result.next();
			System.out.println(document.toJson().toString());

			// LLAMA FECHA EMISION CONVERTIR -> DATE A STRING
			Date IssueDate = document.getDate("IssueDate");
//        SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
			sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
			String fechaPeru = sdfPeru.format(IssueDate);

			// New ADD LLAMA FECHA EMISION CONVERTIR -> STRING A DATE SQL yyyy-MM-dd
			try {////
				date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaPeru);
				date2 = new java.sql.Date(date1.getTime());

			} catch (ParseException e) {
				e.printStackTrace();
			}

			model.addRow(new Object[] { document.getString("identifier"), document.getString("nombre"),
					document.getString("apellido"), document.getString("dni"), document.getBoolean("Enabled"), date2 });

		}

		tabla.setModel(model);

	}

	public void IFListarPorDNIEmpleados(JTable tabla, String dni) {

		DefaultTableModel model = new DefaultTableModel(null,
				new String[] { "Identifier", "Nombre", "Apellido", "DNI", "Habilitado", "Fecha Registro" });

		// SEPARAR LAS COMAS
		String[] dnis = dni.split(",");
		// capturar total para vueltas
		int total = dnis.length;
		System.out.println("Cantidad DNI ingresados: " + total);

		for (int i = 0; i < total; i++) {

			String dnilisto = dnis[i];
			System.out.println("Dni Listo: " + dnilisto);

			DBO = dbConfig.CNXMongoAtlas();
			if (DBO != null) {
				System.out.println("Conexion Ok");
			} else {
				System.out.println("Error Conexi�n");
			}

			Document document = null;
			Bson filter = null;
			Bson Fdni = new Document("dni", dnilisto);
			filter = Filters.and(Fdni);
			System.out.println("filter: " + filter.toString());

			MongoCursor<Document> result = DBO.getCollection("empleado").find(filter).iterator();
			System.out.println(result);
			while (result.hasNext()) {

				document = result.next();
				System.out.println(document.toJson().toString());

				// LLAMA FECHA EMISION CONVERTIR -> DATE A STRING
				Date IssueDate = document.getDate("IssueDate");
//		            SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
				SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
				sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
				String fechaPeru = sdfPeru.format(IssueDate);

				// New ADD LLAMA FECHA EMISION CONVERTIR -> STRING A DATE SQL yyyy-MM-dd
				try {////
					date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaPeru);
					date2 = new java.sql.Date(date1.getTime());

				} catch (ParseException e) {
					e.printStackTrace();
				}

				model.addRow(new Object[] { document.getString("identifier"), document.getString("nombre"),
				document.getString("apellido"), document.getString("dni"), document.getBoolean("Enabled"),
				date2 });
			}
			
			tabla.setModel(model);
		}

	}

	@Override
	public Empleado BuscarByID(String identifier) {
		{

			System.out.println(" EmpleadoDAO -> Method -> BuscarByID  " + "(String " + identifier + ")");

			ArrayList<Empleado> listaEmpleado = new ArrayList<>();
			DBO = dbConfig.CNXMongoAtlas();
			if (DBO != null) {
				System.out.println("Conexion Ok");
			} else {
				System.out.println("Error Conexi�n");
			}
			Document document = null;
			Bson filter = null;
			Bson Fdni = new Document("identifier", identifier);
			filter = Filters.and(Fdni);
			MongoCursor<Document> result = DBO.getCollection("empleado").find(filter).iterator();

			System.out.println(result);
			boolean exist = false;
			while (result.hasNext()) {

				document = result.next();
				System.out.println(document.toJson().toString());

//            CONVERTIR -> DATE  A STRING
				Date IssueDate = document.getDate("IssueDate");
				SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
				sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
				String fechaPeru = sdfPeru.format(IssueDate);
//            CONVERTIR -> STRING A DATE SQL yyyy-MM-dd
				try {
					date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaPeru);
					date2 = new java.sql.Date(date1.getTime());

				} catch (ParseException e) {
					e.printStackTrace();
				}

				String fechaxD = "" + document.getDate("IssueDate");
				System.out.println("Fecha String fechaxD: " + fechaxD);

				emp.setIdentifier(document.getString("identifier"));
				emp.setNombre(document.getString("nombre"));
				emp.setApellido(document.getString("apellido"));
				emp.setDni(document.getString("dni"));
				emp.setEnabled(document.getBoolean("Enabled"));
				emp.setIssueDate(date2);
				listaEmpleado.add(emp);
			}
			return emp;
		}
	}

	@Override
	public void agregar(Empleado empleado) {
		{

			System.out.println("EmpleadoDAO -> metodo -> agregar ");
			String dato1 = empleado.getIdentifier();
			String dato2 = empleado.getNombre();
			String dato3 = empleado.getApellido();
			String dato4 = empleado.getDni();
			Boolean dato5 = empleado.getEnabled();
			Date dato6 = empleado.getIssueDate();
			System.out.println("Obteniendo datos enviados: " + dato1 + " " + dato2 + " " + dato3 + " " + dato4 + " "
					+ dato5 + " " + dato6);

			try {
				DBO = dbConfig.CNXMongoAtlas();
				MongoCollection<Document> collectionEmpleado = DBO.getCollection("empleado");
				Document obj1 = new Document();
				obj1.put("identifier", empleado.getIdentifier());
				obj1.put("nombre", empleado.getNombre());
				obj1.put("apellido", empleado.getApellido());
				obj1.put("dni", empleado.getDni());
				obj1.put("Enabled", empleado.getEnabled());
				obj1.put("IssueDate", empleado.getIssueDate());
				collectionEmpleado.insertOne(obj1);

				System.out.println("Document Insert Successfully using Document Obj...");

			} catch (MongoException | ClassCastException e) {
				System.out.println("Exception occurred while insert Value using **Document**");

			}
		}
	}

	@Override
	public void edit(Empleado e) {
		{

			DBO = dbConfig.CNXMongoAtlas();
			System.out.println("EmpleadoDAO -> metodo -> Edit ");
			MongoCollection<Document> collectionEmpleado = DBO.getCollection("empleado");
//      Bson filter = new Document().append("nombre",e.getNombre());
//      Bson newValue = new Document("identifier", e.getIdentifier());
			Bson filter = Filters.eq("identifier", e.getIdentifier());
			Bson query = Updates.combine(Updates.set("nombre", e.getNombre()), Updates.set("apellido", e.getApellido()),
					Updates.set("dni", e.getDni()), Updates.set("Enabled", e.getEnabled()),
					Updates.set("IssueDate", e.getIssueDate()));

			UpdateResult result = collectionEmpleado.updateOne(filter, query);
		}
	}

	@Override
	public Boolean eliminar(String id) {
		{
			DBO = dbConfig.CNXMongoAtlas();
			System.out.println("EmpleadoDAO -> metodo -> Eliminar ");

			Bson filter = new Document("identifier", id);
			MongoCollection<Document> collectionEmpleado = DBO.getCollection("empleado");
			DeleteResult result = collectionEmpleado.deleteOne(filter);

			Boolean response = false;
			if (result.getDeletedCount() != 0) {
				response = true;
			}
			return response;
		}
	}

	public List listar() {

		System.out.println(" EmpleadoDAO -> metodo -> listar() ");
		ArrayList<Empleado> listaEmpleado = new ArrayList<>();

		DBO = dbConfig.CNXMongoAtlas();
		if (DBO != null) {
			System.out.println("Conexion Ok");
		} else {
			System.out.println("Error Conexi�n");
		}

		Document document = null;
		Bson filter = null;
		// Bson Fdni = new Document("dni", dni);
		// filter= Filters.and(Fdni);

		MongoCursor<Document> result = DBO.getCollection("empleado").find().iterator();
		System.out.println(result);
//        boolean exist=false;

		while (result.hasNext()) {

			document = result.next();
			System.out.println(document.toJson().toString());

			// LLAMA FECHA EMISION CONVERTIR -> DATE A STRING
			Date IssueDate = document.getDate("IssueDate");
//            SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
			sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
			String fechaPeru = sdfPeru.format(IssueDate);

			// New ADD LLAMA FECHA EMISION CONVERTIR -> STRING A DATE SQL yyyy-MM-dd
			try {////
				date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaPeru);
				date2 = new java.sql.Date(date1.getTime());

			} catch (ParseException e) {
				e.printStackTrace();
			}
			// >>>>>>>>>>>>>>>>>

			String fechaxD = "" + document.getDate("IssueDate");
			System.out.println("Fecha String fechaxD: " + fechaxD);

			Empleado cli = new Empleado();
			cli.setIdentifier(document.getString("identifier"));
			cli.setNombre(document.getString("nombre"));
			cli.setApellido(document.getString("apellido"));
			cli.setDni(document.getString("dni"));
			cli.setEnabled(document.getBoolean("Enabled"));
//          cli.setIssueDate(IssueDate);
			cli.setIssueDate(date2);
			listaEmpleado.add(cli);
		}
		return listaEmpleado;
	}

	// Buscar Empleados Por DNI
	@Override
	public List listarByDNI(String dni) {
		System.out.println(" EmpleadoDAO -> metodo -> listarByDNI(String " + dni + ")");
		System.out.println("DNI ingresados: " + dni);

		ArrayList<Empleado> listaEmpleado = new ArrayList<>();

		// SEPARAR LAS COMAS
		String[] dnis = dni.split(",");
		// capturar total para vueltas
		int total = dnis.length;
		System.out.println("Cantidad DNI ingresados: " + total);

		for (int i = 0; i < total; i++) {

			String dnilisto = dnis[i];
			System.out.println("Dni Listo: " + dnilisto);

			DBO = dbConfig.CNXMongoAtlas();
			if (DBO != null) {
				System.out.println("Conexion Ok");
			} else {
				System.out.println("Error Conexi�n");
			}

			Document document = null;
			Bson filter = null;
			Bson Fdni = new Document("dni", dnilisto);
			filter = Filters.and(Fdni);
			System.out.println("filter: " + filter.toString());

			MongoCursor<Document> result = DBO.getCollection("empleado").find(filter).iterator();
			System.out.println(result);
			while (result.hasNext()) {

				document = result.next();
				System.out.println(document.toJson().toString());

				// LLAMA FECHA EMISION CONVERTIR -> DATE A STRING
				Date IssueDate = document.getDate("IssueDate");
//		            SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
				SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
				sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
				String fechaPeru = sdfPeru.format(IssueDate);

				// New ADD LLAMA FECHA EMISION CONVERTIR -> STRING A DATE SQL yyyy-MM-dd
				try {////
					date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaPeru);
					date2 = new java.sql.Date(date1.getTime());

				} catch (ParseException e) {
					e.printStackTrace();
				}

				String fechaxD = "" + document.getDate("IssueDate");
				System.out.println("Fecha String fechaxD: " + fechaxD);

				Empleado cli = new Empleado();
				cli.setIdentifier(document.getString("identifier"));
				cli.setNombre(document.getString("nombre"));
				cli.setApellido(document.getString("apellido"));
				cli.setDni(document.getString("dni"));
				cli.setEnabled(document.getBoolean("Enabled"));
				cli.setIssueDate(date2);

				listaEmpleado.add(cli);

			}

		}

		return listaEmpleado;
	}

}
